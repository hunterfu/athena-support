package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.SupportConfigMapper;
import com.opdar.plugins.mybatis.annotations.Namespace;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/8/7.
 */
@Namespace(SupportConfigMapper.class)
public class SupportConfigEntity {
    private Integer id;
    private String name;
    private Integer stat;
    private String config;
    private String supportId;
    private Timestamp createTime;
    private Timestamp updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
