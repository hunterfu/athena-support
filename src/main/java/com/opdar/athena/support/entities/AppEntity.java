package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.AppConfigMapper;
import com.opdar.athena.support.mapper.AppMapper;
import com.opdar.athena.support.mapper.SupportUserMapper;
import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.plugins.mybatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by shiju on 2017/7/11.
 */
@Namespace(AppMapper.class)
public class AppEntity {
    @Id
    private String id;
    private String company;
    private Timestamp createTime;
    private Timestamp updateTime;
    @Collection(mapper = AppConfigMapper.class,select = "selectList",values = {@Value(key = "appId",value = "id")})
    private List<AppConfigEntity> configs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public List<AppConfigEntity> getConfigs() {
        return configs;
    }

    public void setConfigs(List<AppConfigEntity> configs) {
        this.configs = configs;
    }
}
