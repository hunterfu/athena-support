package com.opdar.athena.support.service;

import com.alibaba.fastjson.JSONObject;
import com.opdar.athena.support.base.Constants;
import com.opdar.athena.support.base.ICacheManager;
import com.opdar.athena.support.entities.ConversationEntity;
import com.opdar.athena.support.entities.SupportConfigEntity;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.mapper.ConversationMapper;
import com.opdar.athena.support.mapper.SupportConfigMapper;
import com.opdar.athena.support.mapper.SupportUserMapper;
import com.opdar.athena.support.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by shiju on 2017/7/20.
 */
@Service
public class MessageService {

    @Autowired
    MessageUtils messageUtils;
    @Autowired
    ConversationMapper conversationMapper;
    @Autowired
    SupportUserMapper supportUserMapper;
    @Autowired
    SupportConfigMapper supportConfigMapper;
    @Autowired
    ICacheManager<String, String, Object> cacheManager;

    public Object send(SupportUserEntity supportUserEntity,String conversationId, String content, Integer type, String reciver, String fakeId, String socktoken, String tip ) {
        String sender;
        String supportId = null;
        String supportMessageId = null;
        if (!StringUtils.isEmpty(conversationId)) {
            ConversationEntity where = new ConversationEntity();
            where.setId(conversationId);
            ConversationEntity conversation = conversationMapper.selectOne(where);
            if (conversation.getSupportUserEntity().getMessageId().equals(reciver)) {
                sender = conversation.getUserEntity().getMessageId();
            } else {
                sender = conversation.getSupportUserEntity().getMessageId();
            }
            if (conversation.getStat() == 2) {
                //会话已结束
                JSONObject object = new JSONObject();
                object.put("type", 999);
                object.put("content", tip);
                object.put("createTime", new Timestamp(System.currentTimeMillis()));
                return object;
            }
            supportId = conversation.getSupportId();
            supportMessageId = conversation.getSupportUserEntity().getMessageId();
        }else{
            //无会话同事沟通
            supportMessageId = sender = supportUserEntity.getMessageId();
            supportId = supportUserEntity.getId();
        }
        return send(content,type,sender,reciver,fakeId,supportId,supportMessageId,socktoken);
    }

    //发消息
    public Object send(String content, Integer type,String sender, String reciver, String fakeId,String supportId,String supportMessageId, String socktoken ) {
        Object message = null;
        {
            message = messageUtils.send(content, type, sender, reciver, fakeId, socktoken);
            Integer userState = (Integer) cacheManager.hget(Constants.USER_STATE, supportId);
            //客服离开状态或免打扰状态，开启自动回复
            if (userState != null && userState > 0 && supportMessageId.equals(reciver)) {
                SupportConfigEntity config = new SupportConfigEntity();
                config.setSupportId(supportId);
                config.setName(Constants.AUTO_REPLY);
                config = supportConfigMapper.selectOne(config);
                if (config != null && config.getStat() != 0 && !StringUtils.isEmpty(config.getConfig())) {
                    JSONObject autoMessage = messageUtils.send(config.getConfig(), type, reciver, sender, UUID.randomUUID().toString(), socktoken);
                    if (!autoMessage.containsKey("code")) {
                        String autoMessageId = autoMessage.getString("id");
                        messageUtils.notify(autoMessageId, socktoken);
                    }
                }
            }
        }
        return message;
    }

    public Object transfer(String sourceId, String conversationId, String reciverId, String remark, String socktoken) {
        ConversationEntity conversation = new ConversationEntity();
        conversation.setId(conversationId);
        conversation = conversationMapper.selectOne(conversation);
        if (conversation != null) {
            ConversationEntity where = new ConversationEntity();
            where.setId(conversationId);
            ConversationEntity update = new ConversationEntity();
            update.setStat(2);
            update.setEndTime(new Timestamp(System.currentTimeMillis()));
            conversationMapper.update(update, where);
            SupportUserEntity supportUser = new SupportUserEntity();
            supportUser.setId(reciverId);
            supportUser = supportUserMapper.selectOne(supportUser);
            if (supportUser != null) {
                ConversationEntity conversationEntity = new ConversationEntity();
                conversationEntity.setId(UUID.randomUUID().toString());
                conversationEntity.setStat(0);
                conversationEntity.setSupportId(reciverId);
                conversationEntity.setUserId(conversation.getUserId());
                conversationEntity.setAppId(conversation.getAppId());
                conversationEntity.setTransfer(1);
                conversationEntity.setSource(sourceId);
                conversationEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
                conversationEntity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
                conversationEntity.setRemark(remark);
                conversationMapper.insert(conversationEntity);
                JSONObject message = messageUtils.send(remark, 998, conversation.getSupportUserEntity().getMessageId(), supportUser.getMessageId(), UUID.randomUUID().toString(), socktoken);
                return message;
            }
        }
        return null;
    }
}
