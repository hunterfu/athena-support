<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <#assign title="Athena客服管理系统 | 登陆"/>
<#include "/resource.ftl"/>
</head>

<body style="background-color: #2f77b0">
<div style="width: 100%;">
    <div id="particles-js"></div>
    <div class="login-form">
        <form class="form-signin">
            <h2 class="form-signin-heading">Athena客服系统</h2>
            <br/>
            <div class="form-inputs">
                <input type="text" name="userName"  v-model="userName" class="form-control" placeholder="请输入账号" required="" autofocus="">
                <input type="password" name="userPwd" v-model="userPwd" class="form-control" placeholder="请输入密码" required="">
            </div>
            <br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">登入</button>
        </form>
    </div>
</div>
<style>
    canvas {
        display: block;
        vertical-align: bottom;
    }

    /* ---- particles.js container ---- */

    #particles-js {
        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0.4;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50% 50%;
        top: 0;
        z-index: -1;
    }

</style>
<script type="text/javascript">
    /* ---- particles.js config ---- */

    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value": 30,
                "density": {
                    "enable": true,
                    "value_area":700
                }
            },
            "color": {
                "value": "#ffffff"
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                }
            },
            "opacity": {
                "value": 0.8,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1,
                    "opacity_min": 0.1,
                    "sync": false
                }
            },
            "size": {
                "value": 10,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 20,
                    "size_min": 1,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 200,
                "color": "#ffffff",
                "opacity": 0.5,
                "width": 2.5
            },
            "move": {
                "enable": true,
                "speed": 1,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "bounce",
                "bounce": true,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": true,
                    "mode": "grab"
                },
                "onclick": {
                    "enable": true,
                    "mode": "push"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 140,
                    "line_linked": {
                        "opacity": 0.7
                    }
                },
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                },
                "repulse": {
                    "distance": 200,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });
</script>
</body>
</html>
